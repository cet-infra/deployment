#!/bin/bash

#### script params! ####

DL_URL="https://bitbucket.org/cet-infra/deployment/downloads/soeassets.zip"
PKG_FILE="soeassets.zip"
LOG="/var/log/install_soeassets.log"

### start logging ###

exec 1> $LOG 2>&1

#### script proper ####
TMP_DIR=`mktemp -d`
echo "$(date) Downloading from $DL_URL to $TMP_DIR/$PKG_FILE..."

curl -LND - "$DL_URL" -o "$TMP_DIR/$PKG_FILE"

echo "$(date) Installing..."
unzip -o "$TMP_DIR/$PKG_FILE" -d /usr/local

#### cleanup ####
echo "$(date) Cleaning up"
rm -R $TMP_DIR