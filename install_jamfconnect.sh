#!/bin/bash

#### script params! ####

APPNAME="JamfConnect"
INSTALL_DETECT="/Applications/Jamf Connect.app"
DL_URL="https://bitbucket.org/cet-infra/deployment/downloads/JamfConnect_2.8.0.pkg"
PKG_FILE="JamfConnect_2.8.0.pkg"
INSTALL_LA_DETECT="/Library/LaunchAgents/com.jamf.connect.plist"
DL_LA_URL="https://bitbucket.org/cet-infra/deployment/downloads/JamfConnectLaunchAgent.pkg"
PKG_LA_FILE="JamfConnectLaunchAgent.pkg"

LOG="/var/log/install_jamfconnect.log"

### start logging ###
exec 1> $LOG 2>&1

TMP_DIR=`mktemp -d`

### Install JAMF Connect if not already installed ###
if [ -e "$INSTALL_DETECT" ] ; then
        echo "$(date) $APPNAME is already installed."
else
        echo "$(date) Downloading from $DL_URL to $TMP_DIR/$PKG_FILE..."
        curl -LND - "$DL_URL" -o "$TMP_DIR/$PKG_FILE"
        echo "$(date) Installing..."
        installer -pkg "$TMP_DIR/$PKG_FILE" -target /
fi

### Install Launch Agent if not already installed ###
if [ -e "$INSTALL_LA_DETECT" ] ; then
        echo "$(date) Launch Agent is already installed."
else
        echo "$(date) Downloading from $DL_LA_URL to $TMP_DIR/$PKG_LA_FILE..."
        curl -LND - "$DL_LA_URL" -o "$TMP_DIR/$PKG_LA_FILE"
        echo "$(date) Installing..."
        installer -pkg "$TMP_DIR/$PKG_LA_FILE" -target /
fi


#### cleanup ####
echo "$(date) Cleaning up"
rm -R $TMP_DIR